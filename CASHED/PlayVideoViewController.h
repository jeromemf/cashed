//
//  PlayVideoViewController.h
//  CASHED
//
//  Created by Jerome on 10/04/14.
//  Copyright (c) 2014 Jerome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Downloader.h"
#import "HTTPServer.h"
#import "DDLog.h"
#import "DDTTYLogger.h"

@protocol PlayVideoViewControllerDelegate <NSObject>

@end

@interface PlayVideoViewController : UIViewController <UITextFieldDelegate, PrepareToPlayDelegate, XMLParserClassDelegate>
{

    HTTPServer *httpServer;
    NSURL *globalURL;
    int from;
    int to;
    double initTime;
}

@property(nonatomic, strong)MPMoviePlayerController *movieController;
@property(nonatomic, strong)AVPlayer *avplayer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic, assign) id <PlayVideoViewControllerDelegate> delegate;

- (IBAction)playVideo:(id)sender;

@end
