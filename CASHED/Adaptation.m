//
//  Adaptation.m
//  CASHED
//
//  Created by Jerome on 30/04/14.
//  Copyright (c) 2014 Jerome. All rights reserved.
//

#import "Adaptation.h"

@implementation Adaptation

- (NSString *)pickFileToDownload:(long)fileSize initialTime:(float)startTime
{
    DeviceProfiler *profiler = [[DeviceProfiler alloc] init];
    XMLParser *parser = [[XMLParser alloc] init];
    
    NSLog(@"#####################################################");
    float tx1 = (fileSize)/(CACurrentMediaTime()- startTime);
    float tx = (tx1*8)/1024;
    NSLog(@"TX = %f Kbit/s", tx);
    NSLog(@"Current Time: %f", CACurrentMediaTime());
    NSLog(@"#####################################################");
    
    NSString *address = @"http://176.111.105.92/videos/svc_1/svc_1.xml";
    NSString *resultAddr = [[NSString alloc] init]; //String used to return the result
    
    [parser parseXML:address];
    
    NSMutableArray *infoArray = [parser parseXML:address];
    long numLayers = [infoArray count]/6;
    
//    NSLog(@"Numero de Layers: %li", numLayers);
    numLayers = numLayers - 1;
    
    /********************************
     * iPhone 4/4s connected to Wifi *
     ********************************/
    
    if (([[profiler checkDevice] isEqualToString:@"IPHONE_4"]    ||
         [[profiler checkDevice] isEqualToString:@"IPHONE_4S"])  &&
        [[profiler checkForNetwork] isEqualToString:@"WiFi"])
    {
        //Code for iPhone 4/4s
        while (numLayers >= 0) {
            if ([[[parser getWidth:infoArray] objectAtIndex:numLayers] intValue] > 720     ||
                [[[parser getHeight:infoArray]objectAtIndex:numLayers] intValue] > 576     ||
                [[[parser getBitRate:infoArray] objectAtIndex:numLayers] intValue] > 12500 ||
                [[[parser getFramerate:infoArray] objectAtIndex:numLayers] intValue] > 30)
            {
//                NSLog(@"LOG: This iPhone 4/4s CANNOT play the Layer %li under WiFi", numLayers);
            }else if([[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue]>tx)
            {
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: Not enought bandwidth to play the layer %li under WiFi on this iPhone 4/4s. ", numLayers);
//                NSLog(@"BitRate: %f Transfer Rate: %f", [[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue], tx);
            }else{
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: This iPhone 4/4s CAN play the layer %li under WiFi", numLayers);
                return resultAddr;
            }
            
            if (numLayers == 0) {
                return @"L0";
            }
            numLayers--;
        }
    }
    /****************************************
     * iPhone 4/4s connected to 3G or lower *
     ***************************************/
    
    if (([[profiler checkDevice] isEqualToString:@"IPHONE_4"]   ||
         [[profiler checkDevice] isEqualToString:@"IPHONE_4S"]) &&
        [[profiler checkForNetwork] isEqualToString:@"3G"])
    {
        //Code for iPhone 4/4s
        while (numLayers >= 0) {
            if ([[[parser getWidth:infoArray] objectAtIndex:numLayers] intValue] > 352     ||
                [[[parser getHeight:infoArray]objectAtIndex:numLayers] intValue] > 288     ||
                [[[parser getBitRate:infoArray] objectAtIndex:numLayers] intValue] > 480   ||
                [[[parser getFramerate:infoArray] objectAtIndex:numLayers] intValue] > 30)
            {
//                NSLog(@"LOG: This iPhone 4/4s CANNOT play the Layer %li under WiFi", numLayers);
            }else if([[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue]>tx)
            {
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: Not enought bandwidth to play the layer %li under 3G on this iPhone 4/4s. ", numLayers);
//                NSLog(@"BitRate: %f Transfer Rate: %f", [[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue], tx);
            }else{
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: This iPhone 4/4s CAN play the layer %li under WiFi", numLayers);
                return resultAddr;
            }
            
            if (numLayers == 0) {
                return @"L0";
            }
            numLayers--;
        }
    }
    /************************************
     * iPhone 5/5c/5s connected to Wifi *
     ***********************************/
    if (([[profiler checkDevice] isEqualToString:@"IPHONE_5"]
         || [[profiler checkDevice] isEqualToString:@"IPHONE_5C"]
         || [[profiler checkDevice] isEqualToString:@"IPHONE_5S"]
         || [[profiler checkDevice] isEqualToString:@"SIMULATOR"])
        && [[profiler checkForNetwork] isEqualToString:@"WiFi"])
    {
        //Code for iPhone 5/5c/5s
        while (numLayers >= 0) {
            if ([[[parser getWidth:infoArray] objectAtIndex:numLayers] intValue] > 1920     ||
                [[[parser getHeight:infoArray]objectAtIndex:numLayers] intValue] > 1088     ||
                [[[parser getBitRate:infoArray] objectAtIndex:numLayers] intValue] > 60000   ||
                [[[parser getFramerate:infoArray] objectAtIndex:numLayers] intValue] > 30.1)
            {
//                NSLog(@"LOG: This iPhone 5/5c/5s CANNOT play the Layer %li under WiFi", numLayers);
            }else if([[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue]>tx)
            {
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: Not enought bandwidth to play the layer %li under WiFi on this iPhone 5/5c/5s. ", numLayers);
//                NSLog(@"BitRate: %f Transfer Rate: %f", [[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue], tx);
            }else{
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: This iPhone 5/5c/5s CAN play the layer %li under WiFi", numLayers);
                return resultAddr;
            }
            
            if (numLayers == 0) {
                return @"L0";
            }
            numLayers--;
        }
    }
    
    /***********************************
     * iPhone 5/5c/5s connected to LTE *
     **********************************/
    
    if (([[profiler checkDevice] isEqualToString:@"IPHONE_5"]
         || [[profiler checkDevice] isEqualToString:@"IPHONE_5C"]
         || [[profiler checkDevice] isEqualToString:@"IPHONE_5S"]
         || [[profiler checkDevice] isEqualToString:@"SIMULATOR"])
        &&[[profiler checkForNetwork] isEqualToString:@"LTE"])
    {
        //Code for iPhone 5/5c/5s
        while (numLayers >= 0) {
            if ([[[parser getWidth:infoArray] objectAtIndex:numLayers] intValue] > 1280     ||
                [[[parser getHeight:infoArray]objectAtIndex:numLayers] intValue] > 720     ||
                [[[parser getBitRate:infoArray] objectAtIndex:numLayers] intValue] > 20000   ||
                [[[parser getFramerate:infoArray] objectAtIndex:numLayers] intValue] > 68.3)
            {
//                NSLog(@"LOG: This iPhone 5/5c/5s CANNOT play the Layer %li under LTE", numLayers);
            }else if([[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue]>tx)
            {
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: Not enought bandwidth to play the layer %li under LTE on this iPhone 5/5c/5s. ", numLayers);
//                NSLog(@"BitRate: %f Transfer Rate: %f", [[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue], tx);
            }else{
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: This iPhone 5/5c/5s CAN play the layer %li under LTE", numLayers);
                return resultAddr;
            }
            
            if (numLayers == 0) {
                return @"L0";
            }
            numLayers--;
        }
    }
    /*******************************************
     * iPhone 5/5c/5s connected to 3G or lower *
     ******************************************/
    
    if (([[profiler checkDevice] isEqualToString:@"IPHONE_5"]
         || [[profiler checkDevice] isEqualToString:@"IPHONE_5C"]
         || [[profiler checkDevice] isEqualToString:@"IPHONE_5S"]
         || [[profiler checkDevice] isEqualToString:@"SIMULATOR"])
        && [[profiler checkForNetwork] isEqualToString:@"3G"])
    {
        //Code for iPhone 5/5c/5s
        while (numLayers >= 0) {
            if ([[[parser getWidth:infoArray] objectAtIndex:numLayers] intValue] > 352     ||
                [[[parser getHeight:infoArray] objectAtIndex:numLayers] intValue] > 288    ||
                [[[parser getBitRate:infoArray] objectAtIndex:numLayers] intValue] > 480   ||
                [[[parser getFramerate:infoArray] objectAtIndex:numLayers] intValue] > 30)
            {
//                NSLog(@"LOG: This iPhone 5/5c/5s CANNOT play the Layer %li under 3G", numLayers);
            }else if([[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue]>tx)
            {
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: Not enought bandwidth to play the layer %li under 3G on this iPhone 5/5c/5s. ", numLayers);
//                NSLog(@"BitRate: %f Transfer Rate: %f", [[[parser getBitRate:infoArray] objectAtIndex:numLayers] floatValue], tx);
            }else{
                resultAddr = [NSString stringWithFormat:@"L%li", numLayers];
//                NSLog(@"LOG: This iPhone 5/5c/5s CAN play the layer %li under 3G", numLayers);
                return resultAddr;
            }
            
            if (numLayers == 0) {
                return @"L0";
            }
            numLayers--;
        }
    }    return nil;
}

@end
