//
//  XMLParser.h
//  CASHED
//
//  Created by Jerome on 27/11/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML.h"

@protocol XMLParserClassDelegate <NSObject>

@end

@interface XMLParser : NSObject{
    NSMutableArray *layersURLArray;
    NSMutableArray *fromToArray;
    NSMutableArray *urlElem;
    NSMutableArray *segmentElem;
    TBXMLElement *rootElem;
    NSMutableArray *elementsArray;
}

@property (nonatomic, assign) id <XMLParserClassDelegate> delegate;

- (NSMutableArray *)parseXML:(NSString *)address;
- (NSMutableArray *)getLayers:(NSMutableArray *)representationInfoArray;
- (NSMutableArray *)getFramerate:(NSMutableArray *)representationInfoArray;
- (NSMutableArray *)getBitRate:(NSMutableArray *)representationInfoArray;
- (NSMutableArray *)getWidth:(NSMutableArray *)representationInfoArray;
- (NSMutableArray *)getHeight:(NSMutableArray *)representationInfoArray;
- (NSString *)getFrom:(NSString *)address;
- (NSString *)getTo:(NSString *)address;
- (NSString *)getName:(NSString *)address;
- (long)getNumberOfLayers:(NSMutableArray *)representationInfoArray;

@end