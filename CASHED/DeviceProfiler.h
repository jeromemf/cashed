//
//  DeviceProfiler.h
//  CASHED
//
//  Created by Jerome on 03/12/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#include <sys/types.h>
#include <sys/sysctl.h>

@protocol DeviceProfilerDelegate <NSObject>

@end

@interface DeviceProfiler : NSObject

@property (nonatomic, assign) id<DeviceProfilerDelegate> profilerDelegate;

- (NSString *)checkDevice;
- (NSString *)checkResolution;
- (NSString *)checkForNetwork;

@end
