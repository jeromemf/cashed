//
//  Adaptation.h
//  CASHED
//
//  Created by Jerome on 30/04/14.
//  Copyright (c) 2014 Jerome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Downloader.h"
#import "DeviceProfiler.h"
#import "PlayVideoViewController.h"

@protocol AdaptationClassDelegate <NSObject>

@end

@interface Adaptation : NSObject
{
    NSMutableData *receivedData;
}

@property (nonatomic, assign) id <AdaptationClassDelegate> delegate;

@property (copy, nonatomic) NSString *userName;

- (NSString *)pickFileToDownload:(long)fileSize initialTime:(float)startTime;

@end
