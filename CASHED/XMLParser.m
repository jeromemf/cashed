//
//  XMLParser.m
//  CASHED
//
//  Created by Jerome on 27/11/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import "XMLParser.h"

//This class parses XML

@implementation XMLParser

- (NSMutableArray *)parseXML:(NSString *)address
{
    /*************************************************
     * METHOD THAT RECEIVES THE ADDRESS OF THE XML TO *
     * PARSE AND CALLS ALL THE OTHER METHODS TO PARSE *
     * INITALIZES:                                    *
     * -THE ARRAY FOR THE BASEURL ELEMENTS OF THE XML *
     * FILE                                           *
     * -THE ARRAY FOR THE SEGMENTINFO ELEMENTS OF THE *
     * XML FILE                                       *
     * -THE ARRAY FOR THE REPRESENTATION ELEMENTS OF  *
     * THE XML FILE                                   *
     * ***********************************************/
    
//    NSError *error;
    NSURL *url = [NSURL URLWithString:address];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSMutableArray *representationElementsArray = [[NSMutableArray alloc] init];
    
    TBXML *sourceXML = [[TBXML alloc] initWithXMLData:data error:nil];
    
//    if (error) {
//        NSLog(@"error: %@ %@", [error localizedDescription], [error userInfo]);
//    } else {
//        NSLog(@"root element: %@", [TBXML elementName:sourceXML.rootXMLElement]);
//    }
    
    rootElem = sourceXML.rootXMLElement;
    
    [self getRespresentationInfo:rootElem representationInfoArray:representationElementsArray];
    
    return elementsArray; //array with the representation elements
}

- (NSMutableArray *)getRespresentationInfo:(TBXMLElement *)element representationInfoArray:(NSMutableArray *)representationInfoElements
{
    do {
        if (element->firstChild)
            [self getRespresentationInfo:element->firstChild representationInfoArray:representationInfoElements];
        
        if ([[TBXML elementName:element] isEqualToString:@"Representation"])
        {
            NSString *codecs = [TBXML valueOfAttributeNamed:@"codecs" forElement:element];
            NSString *frameRate = [TBXML valueOfAttributeNamed:@"frameRate" forElement:element];
            NSString *width = [TBXML valueOfAttributeNamed:@"width" forElement:element];
            NSString *height = [TBXML valueOfAttributeNamed:@"height" forElement:element];
            NSString *bitrate = [TBXML valueOfAttributeNamed:@"bandwidth" forElement:element];
            NSString *layerID = [TBXML valueOfAttributeNamed:@"id" forElement:element];
            [representationInfoElements addObject:codecs];
            [representationInfoElements addObject:frameRate];
            [representationInfoElements addObject:width];
            [representationInfoElements addObject:height];
            [representationInfoElements addObject:bitrate];
            [representationInfoElements addObject:layerID];
        }
    } while ((element = element->nextSibling));
    
    elementsArray = representationInfoElements;
    
    return elementsArray;
}

- (void)traverseXML:(TBXMLElement *)element
{
    /*************************************************
     * METHOD THAT TRAVERSES THE XML FILE AND        *
     * EXTRACTS THE BASEURL ELEMENTS. ADDS THEM TO   *
     * THE URLELEMENTS ARRAY                         *
     *************************************************/
    
    if ([[TBXML elementName:element] isEqualToString:@"BaseURL"])
    {
        [urlElem addObject:[TBXML textForElement:element]];
    }
    //    NSLog(@"Array: %@", second);
    
    do {
        if (element->firstChild)
            [self traverseXML:element->firstChild];
    } while ((element = element->nextSibling));
}

- (NSString *)getName:(NSString *)address
{
    /*************************************************
     * METHOD THAT TRAVERSES THE XML FILE AND        *
     * EXTRACTS THE BASEURL ELEMENTS. ADDS THEM TO   *
     * THE URLELEMENTS ARRAY                         *
     *************************************************/
    
    TBXMLElement *clipElement = [TBXML childElementNamed:@"Clip" parentElement:rootElem];
    TBXMLElement *descriptionElement = [TBXML childElementNamed:@"Description" parentElement:clipElement];
    NSString * description = [TBXML textForElement:descriptionElement];
    
    return description;
}

- (NSString *)getFrom:(NSString *)address
{
    /*************************************************
     * METHOD THAT TRAVERSES THE XML FILE AND        *
     * EXTRACTS THE BASEURL ELEMENTS. ADDS THEM TO   *
     * THE URLELEMENTS ARRAY                         *
     *************************************************/
    
    TBXMLElement *clipElement = [TBXML childElementNamed:@"Clip" parentElement:rootElem];
    TBXMLElement *representationElement = [TBXML childElementNamed:@"Representation" parentElement:clipElement];
    TBXMLElement *segmentInfoElement = [TBXML childElementNamed:@"SegmentInfo" parentElement:representationElement];
    
    NSString *from = [TBXML valueOfAttributeNamed:@"from" forElement:segmentInfoElement];
    
    return from;
}

- (NSString *)getTo:(NSString *)address
{
    //    /*************************************************
    //     * METHOD THAT TRAVERSES THE XML FILE AND        *
    //     * EXTRACTS THE BASEURL ELEMENTS. ADDS THEM TO   *
    //     * THE URLELEMENTS ARRAY                         *
    //     *************************************************/
    
    TBXMLElement *clipElement = [TBXML childElementNamed:@"Clip" parentElement:rootElem];
    TBXMLElement *representationElement = [TBXML childElementNamed:@"Representation" parentElement:clipElement];
    TBXMLElement *segmentInfoElement = [TBXML childElementNamed:@"SegmentInfo" parentElement:representationElement];
    
    NSString *to = [TBXML valueOfAttributeNamed:@"to" forElement:segmentInfoElement];
    
    return to;
}

- (NSMutableArray *)getLayers:(NSMutableArray *)representationInfoArray
{
    int i = 0;
    
    NSMutableArray *layerArray = [[NSMutableArray alloc] init];
    
    while ((i < [representationInfoArray count])) {
        if (i%6 == 5)
        {
            [layerArray addObject:[representationInfoArray objectAtIndex:i]];
        }
        i++;
    }
    return layerArray;
}

- (NSMutableArray *)getFramerate:(NSMutableArray *)representationInfoArray
{
    int i = 0;
    
    NSMutableArray *frameRateArray = [[NSMutableArray alloc] init];
    
    while ((i < [representationInfoArray count])) {
        if (i%6 == 1)
        {
            [frameRateArray addObject:[representationInfoArray objectAtIndex:i]];
        }
        i++;
    }
    return frameRateArray;
}

- (NSMutableArray *)getBitRate:(NSMutableArray *)representationInfoArray
{
    int i = 0;
    
    NSMutableArray *bitRateArray = [[NSMutableArray alloc] init];
    
    while ((i < [representationInfoArray count])) {
        if (i%6 == 4)
        {
            [bitRateArray addObject:[representationInfoArray objectAtIndex:i]];
        }
        i++;
    }
    return bitRateArray;
}

- (NSMutableArray *)getWidth:(NSMutableArray *)representationInfoArray
{
    int i = 0;
    
    NSMutableArray *widthArray = [[NSMutableArray alloc] init];
    
    while ((i < [representationInfoArray count])) {
        if (i%6 == 2)
        {
            [widthArray addObject:[representationInfoArray objectAtIndex:i]];
        }
        i++;
    }
    return widthArray;
}

- (NSMutableArray *)getHeight:(NSMutableArray *)representationInfoArray
{
    int i = 0;
    
    NSMutableArray *heightArray = [[NSMutableArray alloc] init];
    
    while ((i < [representationInfoArray count])) {
        if (i%6 == 3)
        {
            [heightArray addObject:[representationInfoArray objectAtIndex:i]];
        }
        i++;
    }
    return heightArray;
}

- (long)getNumberOfLayers:(NSMutableArray *)representationInfoArray
{
    long numberOfLayers = [representationInfoArray count]/6;    
    return numberOfLayers;
}

@end
