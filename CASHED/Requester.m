//
//  Requester.m
//  CASHED
//
//  Created by Jerome on 10/04/14.
//  Copyright (c) 2014 Jerome. All rights reserved.
//

#import "Requester.h"

@implementation Requester

- (void)getParameters
{
    PrepareToPlay *checkProfiles = [[PrepareToPlay alloc] init]; //initiates an instance of preparetoplay
    
    NSString *parameters = [checkProfiles prepareToPlay];
    
    from = [checkProfiles getFrom];
    to = [checkProfiles getTo];
    
    NSString *f = [NSString stringWithFormat:@"&fm=%d", from];
    NSString *t = [NSString stringWithFormat:@"&to=%d", to];
    
    NSString *finalGet = [[parameters stringByAppendingString:f]stringByAppendingString:t];
    
    NSLog(@"Final Get: %@", finalGet);
    [self requestURL:finalGet];
}

- (void)requestURL:(NSString *)urlAddr
{
    //receives the url returned by the profiler after analisys of the device;
    NSURL *url = [NSURL URLWithString:urlAddr];
    
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    [theRequest setHTTPMethod:@"GET"];
    // Create the NSMutableData to hold the received data.
    // receivedData is an instance variable declared elsewhere.
    receivedData = [NSMutableData dataWithCapacity: 0];
    
    // create the connection with the request
    // and start loading the data
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if (!theConnection) {
        // Release the receivedData object.
        receivedData = nil;
        NSLog(@"The Connection Failed");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse object.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [receivedData appendData:data];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"something very bad happened here");
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    Downloader *downloader = [[Downloader alloc]init];
    
    NSString *responseString = [[NSString alloc] initWithData:receivedData
                                                     encoding:NSUTF8StringEncoding];
    
    NSLog(@"Response String: %@", responseString);
    //    [self playMovie:responseString];
    [downloader downloadFiles:responseString];
}

@end
