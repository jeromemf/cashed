//
//  Downloader.h
//  CASHED
//
//  Created by Jerome on 08/04/14.
//  Copyright (c) 2014 Jerome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceProfiler.h"
#import "PrepareToPlay.h"
#import "Requester.h"
#import "PlayVideoViewController.h"
#import "Adaptation.h"

@protocol DownloaderClassDelegate <NSObject>

@end

@interface Downloader : NSObject
{
    NSFileManager *fileMgr;
    NSArray *paths;
    NSString *documentDirectory;
    NSString *file;
    NSString *file2;
    NSMutableData *receivedData;
    NSString *globalAddr;
    NSString *layerTX;
    int count_down;
    int from;
    int to;
    double currentTime;
    long fileSize;
}

@property (nonatomic, assign) id <DownloaderClassDelegate> delegate;

- (void)downloadFiles:addr;
- (void)startRequest;
- (void)resetCount;

@end

