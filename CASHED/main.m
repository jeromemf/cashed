//
//  main.m
//  CASHED
//
//  Created by Jerome on 08/11/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
