//
//  AppDelegate.h
//  CASHED
//
//  Created by Jerome on 08/11/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
