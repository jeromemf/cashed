//
//  PlayLocalMovieViewController.m
//  CASHED
//
//  Created by Jerome on 08/11/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import "PlayLocalMovieViewController.h"

@interface PlayLocalMovieViewController ()

@end

@implementation PlayLocalMovieViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)PlayVideo:(id)sender
{
    [self startMediaBrowserFromViewController:self usingDelegate:self];
}

- (BOOL)startMediaBrowserFromViewController:(UIViewController *)controller usingDelegate:(id)delegate
{
    if(([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
       || (delegate == nil)
       || (controller == nil)) {
        return NO;
    }
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    //mediaUI.delegate = self;
    
    NSArray *mediaArray = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    
    [mediaUI setMediaTypes:mediaArray];
    //    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    [mediaUI setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];//mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [mediaUI setAllowsEditing:YES];
    [mediaUI setDelegate:delegate];
    [mediaUI setVideoQuality:UIImagePickerControllerQualityTypeHigh];
    
    [self presentViewController:mediaUI animated:YES completion:nil];
    
    return YES;
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
    if(CFStringCompare((__bridge_retained CFStringRef)mediaType, kUTTypeMovie,0) == kCFCompareEqualTo)
    {
        NSURL *movieURL = [info objectForKey:UIImagePickerControllerMediaURL];
        MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
        
        [player prepareToPlay];
        
        [self.view addSubview: player.view];
        [player.view setFrame:self.view.bounds];
        [player setShouldAutoplay:YES];
        [player setControlStyle:MPMovieControlStyleDefault];
        [player setMovieSourceType:MPMovieSourceTypeFile];
        [player setScalingMode:MPMovieScalingModeNone];
        
        if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)==YES) { //To force movie into fullscreen mode (looks better)
            [player setFullscreen:YES  animated:YES];
        } else {
            [player setFullscreen:YES animated:YES];
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(doneButtonClick:)
                                                     name:MPMoviePlayerWillExitFullscreenNotification
                                                   object:player];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playMovieDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:player];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieFinishedCallback:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:player];
        
        self.movieController = player;
        [player play];
    }
}

- (void)doneButtonClick:(NSNotification *)notification //Method to remove the player view when the button 'done'is pressed
{
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerWillExitFullscreenNotification
                                                  object:player];
    
    [player stop];
    [player.view removeFromSuperview];
    
}

- (void)playMovieDidFinish:(NSNotification *)notification //Method to remove the player view when the movie ends
{
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:player];
    
    if([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
}


- (void)movieFinishedCallback:(NSNotification *)notification
{
    [self dismissMoviePlayerViewControllerAnimated];
    MPMoviePlayerController *moviePlayer = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayer];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
