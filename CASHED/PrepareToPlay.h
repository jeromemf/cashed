//
//  PrepareToPlay.h
//  CASHED
//
//  Created by Jerome on 06/12/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLParser.h"
#import "DeviceProfiler.h"

@protocol PrepareToPlayDelegate <NSObject>

@end

@interface PrepareToPlay : NSObject <XMLParserClassDelegate>
{
    int from;
    int to;
    NSString *firstAddr;
    NSString *address;
}

- (NSString *)prepareToPlay;
- (int)getFrom;
- (int)getTo;

@end
