//
//  DeviceProfiler.m
//  CASHED
//
//  Created by Jerome on 03/12/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import "DeviceProfiler.h"

//This class verifies which device is being used, the network it's using and the screen resolution

@implementation DeviceProfiler

- (NSString*)hardwareString
{
    size_t size = 100;
    char *hw_machine = malloc(size);
    int name[] = {CTL_HW,HW_MACHINE};
    sysctl(name, 2, hw_machine, &size, NULL, 0);
    NSString *hardware = [NSString stringWithUTF8String:hw_machine];
    free(hw_machine);
    return hardware;
}

//- (void)checkProfile
//{
//    [self checkDevice];
//    [self checkForNetwork];
//}

- (NSString *)checkDevice
{
    NSString *deviceType = [self hardwareString];
    NSString *deviceModel = [[NSString alloc] init];
    
    if ([deviceType isEqualToString:@"iPhone1,1"]
        || [deviceType isEqualToString:@"iPhone1,2"]
        || [deviceType isEqualToString:@"iPhone2,1"])       deviceModel = @"Legacy";
    
    if ([deviceType isEqualToString:@"iPhone3,1"]
        || [deviceType isEqualToString:@"iPhone3,2"]
        || [deviceType isEqualToString:@"iPhone3,3"])       deviceModel = @"IPHONE_4";
    
    if ([deviceType isEqualToString:@"iPhone4,1"])          deviceModel = @"IPHONE_4S";
    
    if ([deviceType isEqualToString:@"iPhone5,1"]
        || [deviceType isEqualToString:@"iPhone5,2"])       deviceModel = @"IPHONE_5";
    
    if ([deviceType isEqualToString:@"iPhone5,3"]
        || [deviceType isEqualToString:@"iPhone5,4"])       deviceModel = @"IPHONE_5C";
    
    if ([deviceType isEqualToString:@"iPhone6,1"]
        || [deviceType isEqualToString:@"iPhone6,2"])       deviceModel = @"IPHONE_5S";
    
    if ([deviceType isEqualToString:@"i386"]
        || [deviceType isEqualToString:@"x86_64"])          deviceModel = @"SIMULATOR";
    
    // else deviceModel = @"None";
    
    return deviceModel;
}

- (NSString *)checkForNetwork
{
    NSString *networkType = [[NSString alloc] init];
    // check if we've got network connectivity
    Reachability *myNetwork = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus myStatus = [myNetwork currentReachabilityStatus];
    CTTelephonyNetworkInfo *network = [[CTTelephonyNetworkInfo alloc] init];
    
    switch (myStatus) {
        case NotReachable:
            networkType = @"NotReachable";
            NSLog(@"There's no internet connection at all. Display error message now.");
            break;
            
        case ReachableViaWWAN:
            
            //Check the current Radio Acess Technology being used
            if ([network.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyLTE]) {
                networkType = @"LTE";
                //NSLog(@"We have a mobile connection: LTE");
            } else {
                networkType = @"3G";
                //NSLog(@"We have a mobile connection: 3G or lower");
            }
            break;
            
        case ReachableViaWiFi:
            networkType = @"WiFi";
            //NSLog(@"We have WiFi.");
            break;
            
        default:
            break;
    }
    return networkType;
}

- (NSString *)checkResolution
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    CGSize screenSize = CGSizeMake(screenBounds.size.width * screenScale, screenBounds.size.height * screenScale);
    
    //NSLog(@"Resolution: %.0fx%.0f", screenSize.width, screenSize.height);
    NSString *resolution = [NSString stringWithFormat:@"%.0fx%.0f", screenSize.width, screenSize.height];
    
    return resolution;
}

@end
