//
//  PlayVideoViewController.m
//  CASHED
//
//  Created by Jerome on 10/04/14.
//  Copyright (c) 2014 Jerome. All rights reserved.
//

#import "PlayVideoViewController.h"

@interface PlayVideoViewController ()

@property (weak, nonatomic) IBOutlet UILabel *myLoadingLabel;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) UIAlertView *errorView;
@property (strong, nonatomic) UIAlertView *nameAlert;

@end

static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@implementation PlayVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self redirectNSLog];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startPlayOut:)
                                                 name:@"startPlaying"
                                               object:nil];
    
    [self startHTTPServer];
    [self netAvailable];
    
}

- (void)startHTTPServer
{
    // Configure our logging framework.
	// To keep things simple and fast, we're just going to log to the Xcode console.
	[DDLog addLogger:[DDTTYLogger sharedInstance]];
	
	// Initalize our http server
	httpServer = [[HTTPServer alloc] init];
	
	// Tell the server to broadcast its presence via Bonjour.
	// This allows browsers such as Safari to automatically discover our service.
	[httpServer setType:@"_http._tcp."];
    
	// Normally there's no need to run our server on any specific port.
	// Technologies like Bonjour allow clients to dynamically discover the server's port at runtime.
	// However, for easy testing you may want force a certain port so you can just hit the refresh button.
	[httpServer setPort:12345];
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
	// Serve files from our embedded Web folder
	// #NSString *webPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Web"];
	DDLogInfo(@"Setting document root: %@", documentDirectory);
	
	[httpServer setDocumentRoot:documentDirectory];
    
    NSError *error;
	if([httpServer start:&error])
	{
		DDLogInfo(@"Started HTTP Server on port %hu", [httpServer listeningPort]);
	}
	else
	{
		DDLogError(@"Error starting HTTP Server: %@", error);
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playVideo:(id)sender
{
    initTime = CACurrentMediaTime();
    [self netAvailable];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //    [self isNetworkAvailable];
    Downloader *download = [[Downloader alloc] init];
    [download startRequest];
    self.myLoadingLabel.text = @"Loading Movie.... Please wait";
    //UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center = CGPointMake(160, 375);
    self.spinner.tag = 12;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

-(void)startPlayOut:(NSNotification *)notification //Starts the PlayOut after the notification center notifies this method to start.
{
    NSURL *videoURL = [NSURL URLWithString:@"http://127.0.0.1:12345/myVideoFile-.m3u8"];
    [self player:videoURL];
//    NSLog(@"****************************************************************");
//    NSLog(@"*");
//    NSLog(@"*");
//    NSLog(@"*           Startup Time = %f s             ",(CACurrentMediaTime() - initTime));
//    NSLog(@"*");
//    NSLog(@"*");
//    NSLog(@"****************************************************************");
}

- (void)player:(NSURL *)outputURL
{
    [self.activityView stopAnimating];
//    NSLog(@"URL to play: %@",outputURL);

    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL: outputURL];
    
    [player prepareToPlay];
    [self.view addSubview: player.view];
    [player.view setFrame:self.view.bounds];
    [player setMovieSourceType:MPMovieSourceTypeStreaming];
    [player setShouldAutoplay:YES];
    [player setControlStyle:MPMovieControlStyleDefault];
    [player setFullscreen:YES animated:YES];
    [player setScalingMode:MPMovieScalingModeNone];
    
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)==YES) { //To force movie into fullscreen mode (looks better)
        [player setFullscreen:YES  animated:YES];
    } else {
        [player setFullscreen:YES animated:YES];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doneButtonClick:)
                                                 name:MPMoviePlayerWillExitFullscreenNotification
                                               object:player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playMovieDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:player];
    
    self.movieController = player;
    [player play];
    self.myLoadingLabel.text = @"";
    [self.spinner stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)doneButtonClick:(NSNotification *)notification //Method to remove the player view when the button 'done'is pressed
{
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerWillExitFullscreenNotification
                                                  object:player];
    
    [player stop];
    [player.view removeFromSuperview];
    
//    NSString *docsDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
//    NSFileManager *localFileManager=[[NSFileManager alloc] init];
//    NSDirectoryEnumerator *dirEnum = [localFileManager enumeratorAtPath:docsDir];
//    
//    NSString *file;
//    NSError *error;
//    while ((file = [dirEnum nextObject]))
//    {
//        NSString *fullPath = [NSString stringWithFormat:@"%@/%@", docsDir,file];
//        // process the document
//        [localFileManager removeItemAtPath: fullPath error:&error ];
//    }
    Downloader *download = [[Downloader alloc] init];
    [download resetCount];
}

- (void)playMovieDidFinish:(NSNotification *)notification //Method to remove the player view when the movie ends
{
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:player];
    
    if([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    
//    NSString *docsDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
//    NSFileManager *localFileManager=[[NSFileManager alloc] init];
//    NSDirectoryEnumerator *dirEnum = [localFileManager enumeratorAtPath:docsDir];
    
//    NSString *file;
//    NSError *error;
//    while ((file = [dirEnum nextObject]))
//    {
//        NSString *fullPath = [NSString stringWithFormat:@"%@/%@", docsDir,file];
//        // process the document
//        [localFileManager removeItemAtPath: fullPath error:&error ];
//    }
    Downloader *download = [[Downloader alloc] init];
    [download resetCount];
    
    
}

- (void)netAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if(internetStatus == NotReachable) {
        //        UIAlertView *errorView;
        self.errorView = [[UIAlertView alloc]
                          initWithTitle: NSLocalizedString(@"Network error", @"Network error")
                          message: NSLocalizedString(@"No internet connection found, this application requires an internet connection to gather the data required.\n The app will close.", @"Network error")
                          delegate: self
                          cancelButtonTitle: NSLocalizedString(@"Close", @"Network error") otherButtonTitles: nil];
        [self.errorView show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView == self.errorView) {
        if(buttonIndex == 0)
            exit(0);
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.textField) {
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (BOOL)redirectNSLog
{
    // Create log file
    [@"" writeToFile:@"/NSLog.txt" atomically:YES encoding:NSUTF8StringEncoding error:nil];
    id fileHandle = [NSFileHandle fileHandleForWritingAtPath:@"/NSLog.txt"];
    if (!fileHandle)
        return NSLog(@"Opening log failed"), NO;
    
    // Redirect stderr
    int err = dup2([fileHandle fileDescriptor], STDERR_FILENO); if (!err)	return	NSLog(@"Couldn't redirect stderr"), NO;
    return	YES;
}


@end