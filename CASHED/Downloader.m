//
//  Downloader.m
//  CASHED
//
//  Created by Jerome on 08/04/14.
//  Copyright (c) 2014 Jerome. All rights reserved.
//

#import "Downloader.h"
#include <sys/time.h>

@implementation Downloader
int count = -1;

- (void)startRequest
{
    Requester *request = [[Requester alloc] init];
    [request getParameters];
}

- (void)downloadFiles:addr
{
    currentTime = CACurrentMediaTime();
    globalAddr = addr;
//    NSLog(@"COUNT: %d", count);
    
    if (count == -1) {
        NSString *cleanedAddr = [addr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *videoAddress = [cleanedAddr stringByAppendingString:@".m3u8"] ;
        NSURL *url = [[NSURL alloc] initWithString:videoAddress];
//        NSLog(@"URL: %@", url);
        NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:400.0];
        [theRequest setCachePolicy:NSURLCacheStorageAllowedInMemoryOnly];

        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            receivedData = [NSMutableData data] ;
        }
        else {
            NSLog(@"no connection!");
        }
        
    }else {
        NSString *layer = layerTX;
//        NSLog(@"Layer %@", layer);
        NSString *cleanedAddr = [addr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *videoAddress = [cleanedAddr stringByAppendingFormat:@"%@-%d.ts", layer, count];
        NSURL *url = [[NSURL alloc] initWithString:videoAddress];
        NSLog(@"URL: %@", url);
        NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:400.0];
        [theRequest setCachePolicy:NSURLCacheStorageAllowedInMemoryOnly];

        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection) {
            receivedData = [NSMutableData data] ;
        }
        else {
            NSLog(@"no connection!");
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"something very bad happened here...");
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    Adaptation *adapt = [[Adaptation alloc] init];
    layerTX = [adapt pickFileToDownload:[receivedData length] initialTime:currentTime];
    
    PrepareToPlay *checkProfiles = [[PrepareToPlay alloc] init]; //initiates an instance of preparetoplay
    NSString *parameters = [checkProfiles prepareToPlay];
    parameters = @"";
    from = [checkProfiles getFrom];
    to = [checkProfiles getTo];
    
//    NSLog(@"Succeed! Received %ld bytes of data",(unsigned long)[receivedData length]);
    
    fileMgr = [NSFileManager defaultManager];
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentDirectory = [paths objectAtIndex:0];
    //    NSLog(@"COUNT 2: %d", count);
    if (count == -1) {
        file = [documentDirectory stringByAppendingString:@"/myVideoFile-.m3u8"];
    }else{
        file = [documentDirectory stringByAppendingFormat:@"/myVideoFile-%d.ts", count];
    }
    
    if ([fileMgr fileExistsAtPath:file]) {
//        NSLog(@"File already exists. Erasing File");
        [fileMgr removeItemAtPath:file error:nil];
        
//        NSLog(@"Creating File");
        [fileMgr createFileAtPath:file contents:nil attributes:nil];
        
        [receivedData writeToFile:file atomically:NO];
        if ([fileMgr fileExistsAtPath:file]) {
//            NSLog(@"File exists now");
        }else{
//            NSLog(@"File still doesn't exist");
        }
    }else{
//        NSLog(@"File does not exist");
//        NSLog(@"Try creating File");
        
        [fileMgr createFileAtPath:file contents:nil attributes:nil];
        
        [receivedData writeToFile:file atomically:NO];
        
        if ([fileMgr fileExistsAtPath:file]) {
//            NSLog(@"File exists now");
        }else{
//            NSLog(@"File still doesn't exist");
        }
    }
    
    count ++;
    
    if (count <= to) {
        [self downloadFiles:globalAddr];
    }
    
    if (count == 5) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"startPlaying" object:nil];
    }
}

- (void)resetCount
{
    count = -1;
}

@end
