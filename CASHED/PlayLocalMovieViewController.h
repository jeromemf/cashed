//
//  PlayLocalMovieViewController.h
//  CASHED
//
//  Created by Jerome on 08/11/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface PlayLocalMovieViewController : UIViewController

@property (nonatomic) UIImagePickerControllerQualityType videoQuality;

@property(nonatomic, strong)MPMoviePlayerController *movieController;

- (IBAction)PlayVideo:(id)sender;

- (BOOL)startMediaBrowserFromViewController:(UIViewController *)controller usingDelegate:(id)delegate;

@end
