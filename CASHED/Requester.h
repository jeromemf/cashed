//
//  Requester.h
//  CASHED
//
//  Created by Jerome on 10/04/14.
//  Copyright (c) 2014 Jerome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "PrepareToPlay.h"
#import "XMLParser.h"
#import "PlayVideoViewController.h"

@protocol RequesterDelegate <NSObject>

@end

@interface Requester : NSObject
{
    NSMutableData *receivedData;
    int from;
    int to;
//    int count;
//    NSString *first;
//    NSString *generalAddr;
}

@property (nonatomic, assign) id <RequesterDelegate> delegate;

- (void)getParameters;

@end
