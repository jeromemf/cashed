//
//  PrepareToPlay.m
//  CASHED
//
//  Created by Jerome on 06/12/13.
//  Copyright (c) 2013 Jerome. All rights reserved.
//

#import "PrepareToPlay.h"

@implementation PrepareToPlay

- (NSString *)prepareToPlay
{
    XMLParser *parser = [[XMLParser alloc] init];
    
    NSString *ip = @"http://176.111.105.92/";
    address = [ip stringByAppendingString:@"videos/svc_1/svc_1.xml"];
    [parser parseXML:address];
    
    from = [[parser getFrom:address]intValue];
    to = [[parser getTo:address]intValue];
    
    NSString *script = [NSString stringWithFormat:@"scripts/get_parse.py?f=%@",[parser getName:address]];
    firstAddr = [ip stringByAppendingString:script];
    
    return firstAddr;
}

- (int)getFrom
{
    return from;
}

- (int)getTo
{
    return to;
}

@end
